=== UW DGH | SPH Connect ===
Contributors: dghweb, jbleys
Requires at least: 5.8
Tested up to: 6.7
Requires PHP: 7.4.24
License: GNU General Public License v3 or later
License URI: https://www.gnu.org/licenses/gpl-3.0.html

This WordPress plugin facilitates data connection with the UW School of Public Health Administrative and Web Data System API.


== Description ==
This WordPress plugin facilitates data connection with the UW School of Public Health Administrative and Web Data System API.

Data connection is IP restricted or "certificate + private key" restricted. Either option must be verified with the UW School of Public Health before the plugin can be used.


== Changelog ==
**[unreleased]**

#### 0.1.8 / 2025-03-05
* Prevent caching of SPH error returns
* Cached transient can now be cleared individually

#### 0.1.7 / 2025-02-22
* Display certificate valid time, and verify key check
* Bugfix: updated incorrect transient value
* Tested for WP 6.7
* Tested with PHP 8.2

#### 0.1.6 / 2024-03-04
* Added option to connect with SPH's cert-based API
* Tested with PHP 8.1

#### 0.1.5 / 2024-01-10
* Composer updates
* Added Cached Transients table to Tools tab

#### 0.1.4 / 2023-11-30
* Added composer/ca-bundle to fix cURL error 60
* composer updates
* Tested for WP 6.4

#### 0.1.3.2 / 2022-12-23
* Tested for WP 6.1

#### 0.1.3.1 / 2022-07-21
* 'How to' documentation updates

#### 0.1.3 / 2022-07-20
* Code refactoring

#### 0.1.2.2 / 2022-07-20
* Code refactoring

#### 0.1.2.1 / 2022-07-20
* Code refactoring

#### 0.1.2 / 2022-07-12
* Added 'Try it' section to 'How To' tab.

#### 0.1.1 / 2022-04-15
* vendor package updates

#### 0.1 / 2022-04-12
* Initial release

#### 0.0 / 2022-03-18
* Initial commit
