<?php
/*
Plugin Name:           UW DGH | SPH Connect
Plugin URI:            https://bitbucket.org/uwdgh_elearn/uwdgh-sph-connect
Update URI:            https://bitbucket.org/uwdgh_elearn/uwdgh-sph-connect
Description:           This plugin facilitates data connection to the UW School of Public Health administrative system. Data connection is IP restricted or "certificate + private key" restricted. Either option must be verified with the UW School of Public Health before the plugin can be used.
Author:                Department of Global Health - University of Washington
Author URI:            https://depts.washington.edu/dghweb/
Version:               0.1.8
License:               GNU General Public License v3 or later
License URI:           https://www.gnu.org/licenses/gpl-3.0.html
Text Domain:           uwdgh-sph-connect
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-sph-connect

"UW DGH | SPH Connect" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

"UW DGH | SPH Connect" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | SPH Connect". If not, see https://www.gnu.org/licenses/gpl-3.0.html.
 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Basic plugin definitions
 */
if( ! defined( 'UWDGH_SPH_Connect_NAME' ) ) {
	define( 'UWDGH_SPH_Connect_NAME', 'UW DGH | SPH Connect' ); // Name of plugin
}
if( ! defined( 'UWDGH_SPH_Connect_AFFIX' ) ) {
	define( 'UWDGH_SPH_Connect_AFFIX', 'uwdgh_sph_connect' ); // Plugin affix
}
if( ! defined( 'UWDGH_SPH_Connect_DIR' ) ) {
	define( 'UWDGH_SPH_Connect_DIR', plugin_dir_path( __FILE__ ) ); // Plugin dir
}


/**
* UWDGH_SPH_Connect class
*/
if ( !class_exists( 'UWDGH_SPH_Connect' ) ) {

    // includes
    require_once( plugin_dir_path(__FILE__) . 'vendor/autoload.php' );
    include_once( plugin_dir_path(__FILE__) . 'includes/SPH_API_Map.class.php' );
    include_once( plugin_dir_path(__FILE__) . 'includes/SPHConnection.class.php' );

    class UWDGH_SPH_Connect {

        static $plugin;

        /**
        * class constructor
        */
        function __construct()  {

            // Add plugin settings link to Plugins page
            self::$plugin = plugin_basename( __FILE__ );
            add_filter( "plugin_action_links_".self::$plugin, array( __CLASS__, 'add_settings_link' ) );

            // add menu item
            add_action( 'admin_menu' , array( __CLASS__, 'add_menu_item' ));

            // add admin js
            add_action('admin_enqueue_scripts', array( __CLASS__, 'uwdgh_sph_connect_admin_enqueue' ));

            // add ajax callback handler
            add_action( 'wp_ajax_uwdgh_sph_connect_howto_tryit', array( __CLASS__, 'uwdgh_sph_connect_howto_tryit' ) );

            // register plugin settings
            register_setting(
                'uwdgh_sph_connect_options',
                'uwdgh_sph_connect_api',
                array(
                'type' => 'string',
                'description' => 'SPH API Service',
                'default' => 'test_ip',
                )
            );
            register_setting(
                'uwdgh_sph_connect_options',
                'uwdgh_sph_connect_transient_expiration',
                array(
                'type' => 'number',
                'description' => 'transient expiration',
                'default' => 24,
                )
            );
            register_setting(
                'uwdgh_sph_connect_options',
                'uwdgh_sph_connect_cert',
                array(
                'type' => 'string',
                'description' => 'CERTIFICATE in PEM format',
                'default' => '',
                )
            );
            register_setting(
                'uwdgh_sph_connect_options',
                'uwdgh_sph_connect_ssl_key',
                array(
                'type' => 'string',
                'description' => 'PRIVATE KEY in PEM format',
                'default' => '',
                )
            );

            // register deactivation hook
            if( function_exists('register_deactivation_hook') ) {
                register_deactivation_hook(__FILE__,array( __CLASS__, 'deactivate' ));
            }
            // register uninstall hook
            if( function_exists('register_uninstall_hook') ) {
                register_uninstall_hook(__FILE__,array( __CLASS__, 'uninstall' ));
            }

        }

            /**
        * Enqueue admin js
        */
        static function uwdgh_sph_connect_admin_enqueue($hook) {
            // Only add to this plugin's admin page.
            if ('settings_page_uwdgh-sph-connect' != $hook) {
                return;
            }
            wp_enqueue_script( 'uwdgh_sph_connect_admin', plugin_dir_url(__FILE__) . '/js/uwdgh-sph-connect-admin.js', array('jquery') );

            // localize script with 'UWDGH_SPH_Connect' object and 'admin_ajax_url' key => value.
            // the uwdgh-sph-connect-admin file uses the value in
            // UWDGH_SPH_Connect.admin_ajax_url to perform the jQuery Post.
            $UWDGH_SPH_Connect = array( 'admin_ajax_url' => admin_url('admin-ajax.php') );
            wp_localize_script( 'uwdgh_sph_connect_admin', 'UWDGH_SPH_Connect', $UWDGH_SPH_Connect );
        }

        /**
        * Add settings link
        */
        static function add_settings_link( $links ) {
            $settings_link = '<a href="options-general.php?page=uwdgh-sph-connect">' . __( 'Settings' ) . '</a>';
            array_push( $links, $settings_link );
            return $links;
        }

        /**
        * Admin menu item
        */
        static function add_menu_item() {
            add_options_page(
                    __('UW DGH | SPH Connect', 'uwdgh-sph-connect'),	//page title
                    __('UW DGH | SPH Connect', 'uwdgh-sph-connect')	//menu title
                    ,'manage_options'	//capability
                    ,'uwdgh-sph-connect',	//menu_slug
                    array( __CLASS__, 'options_settings_page' )	//callback function
                );
            add_action( "uwdgh_sph_connect_about_content", array( __CLASS__, 'uwdgh_sph_connect_about_content' ) );
            add_action( "settings_content_".self::$plugin, array( __CLASS__, 'options_settings_page_content' ) );
            add_action( "uwdgh_sph_connect_tools_content", array( __CLASS__, 'uwdgh_sph_connect_tools_content' ) );
            add_action( "uwdgh_sph_connect_howto_content", array( __CLASS__, 'uwdgh_sph_connect_howto_content' ) );
        }

        /**
        * Settings page
        */
        static function options_settings_page() {
            // check user capabilities
            if ( ! current_user_can( 'manage_options' ) ) {
            return;
            }
            // Get the active tab from the $_GET param
            $default_tab = null;
            $tab = isset($_GET['tab']) ? $_GET['tab'] : $default_tab;
            ?>
            <div class="wrap">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
                    <nav class="nav-tab-wrapper">
                        <a href="?page=uwdgh-sph-connect" class="nav-tab <?php if($tab===null):?>nav-tab-active<?php endif; ?>">About</a>
                        <a href="?page=uwdgh-sph-connect&tab=settings" class="nav-tab <?php if($tab==='settings'):?>nav-tab-active<?php endif; ?>">Settings</a>
                        <a href="?page=uwdgh-sph-connect&tab=tools" class="nav-tab <?php if($tab==='tools'):?>nav-tab-active<?php endif; ?>">Tools</a>
                        <a href="?page=uwdgh-sph-connect&tab=howto" class="nav-tab <?php if($tab==='howto'):?>nav-tab-active<?php endif; ?>">How to</a>
                    </nav>
                    <div class="tab-content">
                <?php switch($tab) :
                    case 'settings':
                    do_action( "settings_content_".self::$plugin );
                    break;
                    case 'tools':
                    do_action( "uwdgh_sph_connect_tools_content" );
                    break;
                    case 'howto':
                    do_action( "uwdgh_sph_connect_howto_content" );
                    break;
                    default:
                    do_action( "uwdgh_sph_connect_about_content" );
                    break;
                endswitch; ?>
                </div>
            </div>
            <?php
        }

        /**
        * About content
        */
            static function uwdgh_sph_connect_about_content() {
                ?>
                <div class="card">
                    <p><?php _e('This WordPress plugin is for developers/site builders. It facilitates data connection with the UW School of Public Health Administrative and Web Data System API, in particular the Faculty Bio data JSON API.','uwdgh-sph-connect');?></p>
                    <p><?php _e('The ','uwdgh-sph-connect');?><a href="<?php echo admin_url( 'options-general.php?page=uwdgh-sph-connect&tab=howto' ); ?>" class="">"How to"</a><?php _e(' tab explains how to use this plugin.','uwdgh-sph-connect');?></p>
                    <p><?php _e('Data connection to the API is IP restricted or "certificate + private key" restricted. Either option must be verified with the UW School of Public Health before the plugin can be used.','uwdgh-sph-connect');?></p>
                    <p><?php _e('UW School of Public Health contact person is David Grayston (davidtg@uw.edu).','uwdgh-sph-connect');?></p>
                    <p><?php _e('This plugin was developed by the UW Department of Global Health web team (dghweb@uw.edu).','uwdgh-sph-connect');?></p>
                </div>
                <?php
            }

        /**
        * Settings page content
            *
        */
        static function options_settings_page_content() {
            ?>
            <h3>Settings</h3>
            <p><?php _e('Here you can set or edit the fields needed for the plugin.','uwdgh-sph-connect');?></p>
            <form action="options.php" method="post" id="uwdgh-sph-connect-options-form">
            <?php settings_fields('uwdgh_sph_connect_options'); ?>
                <table class="form-table">
                    <tr class="odd" valign="top">
                        <th scope="row">
                            <label for="uwdgh_sph_connect_api">
                                <?php _e('SPH API Service','uwdgh-sph-connect');?>
                            </label>
                        </th>
                        <td>
                        <select id="uwdgh_sph_connect_api" name="uwdgh_sph_connect_api">
                            <option value="production_cert" <?php selected( get_option('uwdgh_sph_connect_api'), 'production_cert' ); ?>>Production (Cert-based service)</option>
                            <option value="production_ip" <?php selected( get_option('uwdgh_sph_connect_api'), 'production_ip' ); ?>>Production (IP-based service)</option>
                            <option value="test_ip" <?php selected( get_option('uwdgh_sph_connect_api'), 'test_ip' ); ?>>Test/dev (IP-based service)</option>
                        </select>
                        <p class="description"><?php _e('Unless working in a development environment this option should be set to a "Production" service.','uwdgh-sph-connect');?></p>
                        </td>
                    </tr>
                    <tr class="even" valign="top">
                        <th scope="row">
                            <label for="uwdgh_sph_connect_transient_expiration">
                                <?php _e('Cache expiration','uwdgh-sph-connect');?>
                            </label>
                        </th>
                        <td>
                            <select id="uwdgh_sph_connect_transient_expiration" name="uwdgh_sph_connect_transient_expiration">
                                <option value="0.0166666666666667" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 0.0166666666666667 ); ?>>1 minute (not recommended)</option>
                                <option value="12" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 12 ); ?>>12 hours</option>
                                <option value="24" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 24 ); ?>>1 day (24 hours)</option>
                                <option value="48" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 48 ); ?>>2 days</option>
                                <option value="72" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 72 ); ?>>3 days</option>
                                <option value="96" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 96 ); ?>>4 days</option>
                                <option value="120" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 120 ); ?>>5 days</option>
                                <option value="168" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 168 ); ?>>1 week</option>
                                <option value="336" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 336 ); ?>>2 weeks</option>
                                <option value="504" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 504 ); ?>>3 weeks</option>
                                <option value="672" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 672 ); ?>>4 weeks</option>
                                <option value="4392" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 4392 ); ?>>½ year</option>
                                <option value="8760" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 8760 ); ?>>1 year</option>
                                <option value="0" <?php selected( get_option('uwdgh_sph_connect_transient_expiration'), 0 ); ?>>Persistent</option>
                            </select>
                            <p class="description"><?php _e('SPH requests are cached to optimize page loading. When the cache timeout expires, a new request to the SPH system will refresh the data.','uwdgh-sph-connect');?></p>
                        </td>
                    </tr>
                    <tr class="odd" valign="top">
                        <th scope="row">
                            <label for="uwdgh_sph_connect_cert">
                                <?php _e('CERTIFICATE','uwdgh-sph-connect');?>
                            </label><br>
                            <span class="required"><?php _e('* Required with the Cert-based API service', 'uwdgh-sph-connect'); ?></span>
                        </th>
                        <td>
                            <textarea style="font-family:Courier, monospace;" id="uwdgh_sph_connect_cert" name="uwdgh_sph_connect_cert" rows="10" cols="65" placeholder=""><?php echo esc_textarea( get_option('uwdgh_sph_connect_cert') ); ?></textarea>
                            <p class="description"><?php _e('CERTIFICATE in PEM format', 'uwdgh-sph-connect'); ?></p>
                            <?php
                            if ( !empty( get_option('uwdgh_sph_connect_cert') ) ) {
                                $x509_data = openssl_x509_parse( get_option('uwdgh_sph_connect_cert') );
                                $validFrom = date('Y-m-d', $x509_data['validFrom_time_t']);
                                $validTo = date('Y-m-d', $x509_data['validTo_time_t']);
                            }
                            ?>
                            <?php if ( is_array($x509_data) ) : ?>
                                <p class=""><?php _e('Valid time', 'uwdgh-sph-connect'); ?>: <samp><strong><time><?php echo $validFrom; ?></time> &ndash; <time><?php echo $validTo; ?></time></strong></samp></p>
                                <details class="">
                                    <summary><?php _e('Certificate details', 'uwdgh-sph-connect'); ?></summary>
                                    <textarea rows="10" cols="65" disabled style="font-family:Courier, monospace;" ><?php print_r($x509_data); ?></textarea>
                                </details>
                            <?php elseif ( empty( get_option('uwdgh_sph_connect_cert') ) ) : ?>
                            <?php else : ?>
                                <p><span class="required"><?php _e('‼ Not a valid certificate', 'uwdgh-sph-connect'); ?></span></p>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr class="even" valign="top">
                        <th scope="row">
                            <label for="uwdgh_sph_connect_ssl_key">
                                <?php _e('SSL KEY','uwdgh-sph-connect');?>
                            </label><br>
                            <span class="required"><?php _e('* Required with the Cert-based API service', 'uwdgh-sph-connect'); ?></span>
                        </th>
                        <td>
                            <textarea style="font-family:Courier, monospace;" id="uwdgh_sph_connect_ssl_key" name="uwdgh_sph_connect_ssl_key" rows="10" cols="65" placeholder=""><?php echo esc_textarea( get_option('uwdgh_sph_connect_ssl_key') ); ?></textarea>
                            <p class="description"><?php _e('PRIVATE KEY in PEM format', 'uwdgh-sph-connect'); ?></p>
                            <?php
                            if ( !empty( get_option('uwdgh_sph_connect_ssl_key') || !empty( get_option('uwdgh_sph_connect_cert') ) ) ) {
                                $x509_check_private_key = openssl_x509_check_private_key( get_option('uwdgh_sph_connect_cert'), get_option('uwdgh_sph_connect_ssl_key') );
                                ?>
                                <p class=""><?php echo ($x509_check_private_key) ? __('✔ key corresponds to certificate', 'uwdgh-sph-connect') : __('<span class="required">‼ key does not correspond to certificate</span>', 'uwdgh-sph-connect'); ?></p>
                                <?php
                            }
                            ?>
                        </td>
                    </tr>
                </table>
                <?php submit_button(); ?>
            </form>
            <?php
        }

        /**
        * Test connection content
        */
        static function uwdgh_sph_connect_tools_content() {
            // get the values from the querystring parameters
            // API test
            if(isset($_GET['sphcm-api-test'])) {
                if ($_GET['sphcm-api-test'] == 'true') {
                    if  ( wp_verify_nonce($_GET['_wpnonce'], 'sphcm-api-test') != false ) {
                        $args = array(
                            'endpoint' => 'faclistDept',
                            'parameter' => array(
                                'dept_ID' => 'glbhlth',
                            ),
                        );
                        do_action( 'sphconnection_sphcm_api_test', $args );
                    } else {
                        \UWDGH\SPHConnection::error_notice('Error Communicating with Server.');
                    }
                }
            }
            // Clear cache
            if(isset($_GET['uwdgh-sph-connect-clear-cache'])) {
                if ( $_GET['uwdgh-sph-connect-clear-cache'] == 'true' ) {
                    if ( wp_verify_nonce($_GET['_wpnonce'], 'uwdgh-sph-connect-clear-cache') != false ) {
                        self::uwdgh_sph_connect_clear_transient_cache( isset($_GET['key']) ? $_GET['key'] : null );
                        \UWDGH\SPHConnection::success_notice('Cache cleared.');
                    } else {
                        \UWDGH\SPHConnection::warning_notice('There was an issue. Cache has not been cleared.');
                    }
                }
            }
        ?>
            <h3>Tools</h3>
            <table class="form-table">
                <tr valign="top" hidden style="display:none;visibility:hidden;">
                    <th>API connection test:</th>
                    <td><a href="<?php echo self::uwdgh_sph_connect_testlink(); ?>" class="button button-secondary"><?php _e('Run&hellip;','uwdgh-sph-connect');?></a></td>
                </tr>
                <tr valign="top">
                    <th>Clear transient cache:</th>
                    <td><a href="<?php echo self::uwdgh_sph_connect_cclink(); ?>" class="button button-primary button-large"><?php _e('Clear all cache','uwdgh-sph-connect');?></a></td>
                </tr>
            </table>			
            <h4>Cached Transients</h4>
            <div style="max-height:480px;overflow-y:scroll;">
                <table class="widefat striped fixed">
                    <thead>
                        <tr>
                            <th style="width:30%;"><?php _e('Name','uwdgh-sph-connect');?></th>
                            <th><?php _e('Value','uwdgh-sph-connect');?></th>
                            <th style="width:10%;"><?php _e('Action','uwdgh-sph-connect');?></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        global $wpdb;
                        $results = $wpdb->get_results(
                            $wpdb->prepare(
                                "SELECT `option_name` AS `name`, `option_value` AS `value` FROM $wpdb->options WHERE option_name LIKE %s",
                                '%_transient_uwdgh_sph_connect_%'
                            )
                        );
                    ?>
                    <?php
                        foreach ( $results as $result ) {
                    ?>
                            <tr>
                                <td><?php echo $result->name;?></td>
                                <td>
                                    <details>	
                                        <summary style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis; cursor: pointer;"><code><?php echo substr($result->value, 0, 128).'&hellip;';?></code></summary>
                                        <pre style="max-height: 360px;overflow: auto; white-space: pre-line;"><?php echo $result->value; ?></pre>
                                        </details>
                                </td>
                                <td><a href="<?php echo self::uwdgh_sph_connect_cclink( $result->name ); ?>" class="button button-secondary button-small"><?php _e('clear cache','uwdgh-sph-connect');?></a></td>
                            </tr>
                    <?php
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        <?php
        }

        /**
        * How to content
        */
        static function uwdgh_sph_connect_howto_content() {
            $args = array(
                'endpoint' => 'faclistDept',
                'parameter' => array(
                    'dept_ID' => 'glbhlth',
                    'reg' => 'true',
                ),
            );
            $args2 = array(
                'endpoint' => 'facDetail',
                'parameter' => array(
                    'url_ID' => 'Pfeiffer_James',
                    'web_publ_ID' => 'glbhlth',
                ),
            );
            $placeholder = json_encode($args, JSON_PRETTY_PRINT);
            $placeholder = str_replace("\\r\\n", "\\\\n",$placeholder);
            $placeholder2 = json_encode($args2, JSON_PRETTY_PRINT);
            $placeholder2 = str_replace("\\r\\n", "\\\\n",$placeholder2);
            ?>
            <h3>How to</h3>
            <p>How to use this plugin.</p>
            <h4>Intro</h4>
            <ul style="list-style: inside;margin-left: 1.5em;">
            <li>This plugin adds a hook that can be used for requests to the SPH system.</li>
                <li>Use this hook in your custom plugin or theme.</li>
                <li>The hook is a <a href="https://developer.wordpress.org/plugins/hooks/filters/" title="Filters | Plugin Developer Handbook | WordPress Developer Resources" target="_blank" rel="noopener noreferrer">filter</a> and returns a JSON string value.</li>
            </ul>
            <h4>Usage</h4>
            <pre><code>$value = apply_filters( 'sph_get_faculty_data', null, $args );</code></pre>
            <ul style="list-style: inside;margin-left: 1.5em;">
                <li><samp>$value</samp> is your variable for the returned result, which is a JSON string notation.</li>
                <li><samp>apply_filters()</samp> is the WordPress <a href="https://developer.wordpress.org/reference/functions/apply_filters/" title="apply_filters() | Function | WordPress Developer Resources" target="_blank" rel="noopener noreferrer">function</a> to call the callback function.</li>
                <li><samp>'sph_get_faculty_data'</samp> is the name of the callback function, aka the Hook.</li>
                <li><samp>null</samp> is the input to filter. Since we're not actually filtering any input, this parameter is always null.</li>
                <li><samp>$args</samp> contains the parameters to pass to the callback function.</li>
            </ul>
            <p>In your plugin or theme, declare an arguments array to pass into the hook. For example:</p>
            <table>
                <tr>
                    <td>
        <pre>
        $args = array(
        'endpoint' => 'faclistDept',
        'parameter' => array(
            'dept_ID' => 'glbhlth',
            'reg' => 'true',
            'adj' => 'true',
            'cli' => 'true'
        ),
        );
        </pre>
                    </td>
                    <td style="width:20%;text-align:center;">or:</td>
                    <td>
        <pre>
        $args = array(
        'endpoint' => 'facDetail',
        'parameter' => array(
            'url_ID' => 'Pfeiffer_James',
            'web_publ_ID' => 'glbhlth'
        ),
        );
        </pre>
                        </td>
                    </tr>
                </table>
                <p>The <code>$args</code> array has two keys: <code>endpoint</code> and <code>parameter</code>.</p>
                <p>The <code>endpoint</code> value is a string of one of the following values: <samp>faclistDept</samp>, <samp>faclistSPH</samp>, <samp>faclistSPH_Core</samp>, <samp>faclistSPH_Category</samp>, <samp>facDetail</samp>, <samp>ceFac</samp>, <samp>faclistAppt</samp>, <samp>facNameSearch</samp>, <samp>facRiSearch</samp>.<br>
                These strings refer to the specific SPH API data you're targeting. Please refer to the <a href="https://uwnetid-my.sharepoint.com/personal/sphit_uw_edu/appdocs/Documentation/Home.aspx" title="SPH Application and Database Documentation" target="_blank" rel="noopener noreferrer">SPH Application and Database Documentation</a> for details.</p>
                <p>The <code>parameter</code> value is an array of accepted <samp>key => value</samp> pairs for the endpoint. Please refer to the <a href="https://uwnetid-my.sharepoint.com/personal/sphit_uw_edu/appdocs/Documentation/Home.aspx" title="SPH Application and Database Documentation" target="_blank" rel="noopener noreferrer">SPH Application and Database Documentation</a> for parameter specifics.</p>
                <hr>
                <h3>Try it</h3>
                <p>Enter a valid JSON notation of an arguments array below, and click the <strong>Try it&hellip;</strong> button. A placeholder value is displayed as example.</p>
                <table class="form-table">
                    <tr valign="top">
                        <td><textarea id="uwdgh_sph_connect_try_it" name="uwdgh_sph_connect_try_it" cols="40" rows="8" maxlength="65525" placeholder='<?php echo $placeholder; ?>'></textarea><br>
                            <a href="#" id="uwdgh_sph_connect_try_it_link" class="button button-secondary"><?php _e('Try it&hellip;','uwdgh-sph-connect');?></a>&nbsp;
                            <a href="#" id="uwdgh_sph_connect_try_it_clear" class="button button-secondary"><?php _e('Clear','uwdgh-sph-connect');?></a>&nbsp;
                            <span class="spinner" style="float:none;"></span>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <span>Result:</span><br>
                            <textarea id="uwdgh_sph_connect_try_it_result" readonly name="uwdgh_sph_connect_try_it_result" cols="150" rows="20"></textarea>
                        </td>
                    </tr>
                </table>
                <?php
            }

        /**
        * wp_ajax_ action callback
        */
        static function uwdgh_sph_connect_howto_tryit() {
            $inputArray = $_POST['uwdgh_sph_connect_try_it'];	// get the array value
            // call the SPH system
            $retval = apply_filters( 'sph_get_faculty_data', null, $inputArray );
            // echo the response
            echo json_encode($retval, JSON_PRETTY_PRINT);

            // Don't forget to stop execution afterward.
            wp_die();
        }

        /**
        * Create the test link
        */
        private static function uwdgh_sph_connect_testlink() {
            $testlink = get_admin_url( null, 'options-general.php', 'admin' );
            $testlink = add_query_arg( 'page', 'uwdgh-sph-connect', $testlink);
            $testlink = add_query_arg( 'tab', 'tools', $testlink);
            $testlink = add_query_arg( 'sphcm-api-test', 'true', $testlink);
            $testlink = add_query_arg( '_wpnonce', wp_create_nonce( 'sphcm-api-test' ), $testlink );
            return $testlink;
        }
        /**
        * Create the clear-cache link
        */
        private static function uwdgh_sph_connect_cclink( $key = null ) {
            $cclink = get_admin_url( null, 'options-general.php', 'admin' );
            $cclink = add_query_arg( 'page', 'uwdgh-sph-connect', $cclink);
            $cclink = add_query_arg( 'tab', 'tools', $cclink);
            $cclink = add_query_arg( 'uwdgh-sph-connect-clear-cache', 'true', $cclink);
            $cclink = add_query_arg( '_wpnonce', wp_create_nonce( 'uwdgh-sph-connect-clear-cache' ), $cclink );
            if ( $key ) {
                $cclink = add_query_arg( 'key', $key, $cclink);
            }
            return $cclink;
        }

        /**
        * Dispose transient cache for this plugin
        */
        private static function uwdgh_sph_connect_clear_transient_cache( $transient = null ) {
            global $wpdb;
            
            if ( $transient ) {
				$wpdb->query(
					$wpdb->prepare(
                        "DELETE FROM $wpdb->options WHERE option_name LIKE %s",
                        '%'.$transient.'%'
					)
			    );
            } else {
                $wpdb->query(
                    $wpdb->prepare(
                        "DELETE FROM $wpdb->options WHERE option_name LIKE %s",
                        '%_transient_%uwdgh_sph_connect_%'
                    )
                );
            }
        }

        /**
        * Plugin deactivation
        */
        static function deactivate() {
            // remove options
            delete_option('uwdgh_sph_connect_api');
            delete_option('uwdgh_sph_connect_transient_expiration');
            // clear transient cache
            self::uwdgh_sph_connect_clear_transient_cache();
        }

        /**
        * Plugin deletion
        */
        static function uninstall() {
        // nothing to do here
        }

    }

    new UWDGH_SPH_Connect;

}
