<?php
namespace UWDGH;

/**
* Class SPHConnection
*
* @package UW DGH | SPH Connect
*/
if ( !class_exists( 'SPHConnection' ) ) {

  class SPHConnection {

    private static $api_uri;
		private static $fpem;	// file path for the transactional cert file
		private static $fkey;	// file path for the transactional key file

    /**
    * class constructor
    */
    function __construct()  {

			self::set_api();

      // custom hooks
      add_action( 'sphconnection_sphcm_api_test', array( __CLASS__, 'sphcm_api_test' ), 10, 1 );
      add_filter( 'sph_get_faculty_data', array( __CLASS__, 'sphcm_get_facdata' ), 10, 2 );

    }

		/**
		 * Help function
		 * Set active api based on plugin option
		 */
		private static function set_api() {

      switch (get_option('uwdgh_sph_connect_api')) {
				case 'test_ip':
          self::$api_uri = \UWDGH\SPH_API_Map::service()->ip_based->test->uri;
          break;
				case 'production_ip':
					self::$api_uri = \UWDGH\SPH_API_Map::service()->ip_based->production->uri;
					break;
				case 'production_cert':
					self::$api_uri = \UWDGH\SPH_API_Map::service()->cert_based->production->uri;
					break;
        default:
          self::$api_uri = \UWDGH\SPH_API_Map::service()->ip_based->test->uri;
          break;
      }

		}

    /**
    * Callback function for 'sph_get_faculty_data' filter
    */
    static function sphcm_get_facdata( $value = null, $args = array()) {
      try {
				// create transactional pem and key files
				self::create_cert_transaction_files();
				// validate parameters
        self::validate_args($args);
        // Build querystring from paramater array
        $qs = http_build_query($args['parameter']);
        $url = self::$api_uri . $args['endpoint'] . "?$qs";
        $value = self::get_response($url);
        return $value;
      } catch (\Exception $exc) {
        // self::error_notice($exc->getMessage());
        return $exc;
      } finally {
				// delete transactional pem and key files
				self::delete_cert_transaction_files();
			}
			
    }
    /**
    * Validate the args array passed in 'sphcm_get_facdata'
    */
    private static function validate_args( $args = array() ) {
      try {
        // No args, throw error
        if (!$args || !is_array($args)) {
          throw new \Exception("Error Processing Request. Invalid arguments.");
        }
        // No endpoint, throw error
        if ( !array_key_exists($args['endpoint'], \UWDGH\SPH_API_Map::map_array()['endpoint'] ) ) {
          throw new \Exception("Error Processing Request. Invalid endpoint.", 1);
        }
        // No required parameter, throw error
        switch ($args['endpoint']) {
          case 'faclistDept':
            if ( !array_key_exists('dept_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'dept_ID' is required.");
            }
            if ( !in_array($args['parameter']['dept_ID'], \UWDGH\SPH_API_Map::map_array()['endpoint']['faclistDept']['parameter']['dept_ID']['values']) ) {
              throw new \Exception("Error Processing Request. A valid 'dept_ID' parameter is required.");
            }
            break;
          case 'faclistSPH':
            if ( !array_key_exists('dept_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'dept_ID' is required.", 1);
            }
            if ( !in_array($args['parameter']['dept_ID'], \UWDGH\SPH_API_Map::map_array()['endpoint']['faclistSPH']['parameter']['dept_ID']['values']) ) {
              throw new \Exception("Error Processing Request. A valid 'dept_ID' parameter is required.");
            }
            break;
          case 'facDetail':
            if ( !array_key_exists('url_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'url_ID' is required.");
            }
            if ( !array_key_exists('web_publ_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'web_publ_ID' is required.");
            }
            if ( !in_array($args['parameter']['web_publ_ID'], \UWDGH\SPH_API_Map::map_array()['endpoint']['facDetail']['parameter']['web_publ_ID']['values']) ) {
              throw new \Exception("Error Processing Request. A valid 'web_publ_ID' parameter is required.");
            }
            break;
          case 'ceFac':
            if ( !array_key_exists('url_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'url_ID' is required.");
            }
            break;
          case 'faclistAppt':
            if ( !array_key_exists('dept_ID', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'dept_ID' is required.");
            }
            if ( !in_array($args['parameter']['dept_ID'], \UWDGH\SPH_API_Map::map_array()['endpoint']['faclistAppt']['parameter']['dept_ID']['values']) ) {
              throw new \Exception("Error Processing Request. A valid 'dept_ID' parameter is required.");
            }
            break;
          case 'facNameSearch':
            if ( !array_key_exists('nameStr', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'nameStr' is required.");
            }
            break;
          case 'facRiSearch':
            if ( !array_key_exists('srch_phrase', $args['parameter']) ) {
              throw new \Exception("Error Processing Request. Parameter 'srch_phrase' is required.");
            }
            break;
          default:
            // nothing
            break;
        }
      } catch (\Exception $exc) {
        self::error_notice($exc->getMessage());
      }
    }

    /**
    * Central function to handle all requests or cached data
    *
    */
    private static function get_response($url) {
      // transient key cannot be longer than 172 characters
      // so we hash the url part to trim to a fixed length
      $transient_key = UWDGH_SPH_Connect_AFFIX . '_' . hash('sha1',$url);
      try {
        $data = get_transient($transient_key);
        if ($data) {
          // get data from transient cache
          $data = unserialize($data);
        } else {
          // Instantiate client
					switch ( get_option('uwdgh_sph_connect_api') ) {
						case 'production_cert':
							$client = new \GuzzleHttp\Client([
								\GuzzleHttp\RequestOptions::CERT => self::$fpem,
								\GuzzleHttp\RequestOptions::SSL_KEY => self::$fkey,
								\GuzzleHttp\RequestOptions::HEADERS => [
									'Connection' => 'close',
									'User-Agent' => UWDGH_SPH_Connect_NAME.' (mailto:dghweb@uw.edu)']
								]);
							break;
						case 'production_ip':
							$client = new \GuzzleHttp\Client([
								\GuzzleHttp\RequestOptions::VERIFY => \Composer\CaBundle\CaBundle::getSystemCaRootBundlePath(),
								\GuzzleHttp\RequestOptions::HEADERS => [
									'Connection' => 'close',
									'User-Agent' => UWDGH_SPH_Connect_NAME.' (mailto:dghweb@uw.edu)']
								]);
							break;
						default:
							$client = new \GuzzleHttp\Client([
								\GuzzleHttp\RequestOptions::VERIFY => \Composer\CaBundle\CaBundle::getSystemCaRootBundlePath(),
								\GuzzleHttp\RequestOptions::HEADERS => [
									'Connection' => 'close',
									'User-Agent' => UWDGH_SPH_Connect_NAME.' (mailto:dghweb@uw.edu)']
								]);
							break;
					}
          // Get the response
          $response = $client->get($url);
          if ($response->getStatusCode() === 200) {
            // Decode the response body to string
            $data = json_decode((string) $response->getBody(), true); // true = array instead of object
            
            // $data_string = print_r( isset( $data['error'] ), true );
            // error_log($data_string);

            if ( is_array($data) && !isset( $data['error'] ) ) {
              // populate transient cache if not an error, and set cache expiration in seconds
              set_transient($transient_key, serialize($data), 3600 * get_option('uwdgh_sph_connect_transient_expiration'));
            }
          } else {
            return self::error_notice( '<code>'.$response->getStatusCode().'</code>&nbsp;'.$response->getReasonPhrase() );
          }
        }
        // return
        return $data;
      } catch (Exception $exc) {
        self::error_notice($exc->getMessage());
      }
    }

		/**
		 * help function to create the transaction cert files
		 */
		private static function create_cert_transaction_files() {

			if ( 'production_cert' != get_option('uwdgh_sph_connect_api') ) 
				return;

			$_now = date_format(date_create("now"),"Y-m-d\TH:i:s:u");
			$_transactional = (string)rand().UWDGH_SPH_Connect_NAME.$_now;
			$_pem = hash( 'md5', $_transactional ) . '.pem';
			$_key = hash( 'md5', $_transactional ) . '.key';
			self::$fpem = UWDGH_SPH_Connect_DIR . 'ca/' . $_pem;
			self::$fkey = UWDGH_SPH_Connect_DIR . 'ca/' . $_key;

      $fp = fopen( self::$fpem, 'w' );
      fwrite( $fp, get_option('uwdgh_sph_connect_cert') );
      fclose( $fp );
			
      $fk = fopen( self::$fkey, 'w' );
      fwrite( $fk, get_option('uwdgh_sph_connect_ssl_key') );
      fclose( $fk );

		}
		
		/**
		 * help function to delete the transactional cert files
		 */
		private static function delete_cert_transaction_files() {

			if ( 'production_cert' != get_option('uwdgh_sph_connect_api') ) 
				return;
			
			if (file_exists( self::$fpem ))
        unlink( self::$fpem );
			
			if (file_exists( self::$fkey ))
        unlink( self::$fkey );

		}


    /**
    * Hook function sphcm_api_test
    *
    * @param array $args
    * @return string $notice
    *
    * @since 0.1
    */
    static function sphcm_api_test( $args = array() ) {
      try {
        self::validate_args($args);
        // Build querystring from paramater array
        $qs = http_build_query($args['parameter']);
        $url = self::$api_uri . $args['endpoint'] . "?$qs";
        // Set new Client object
        //$client = new \GuzzleHttp\Client();
        $client = new \GuzzleHttp\Client(
					[\GuzzleHttp\RequestOptions::VERIFY => \Composer\CaBundle\CaBundle::getSystemCaRootBundlePath()]
				);
        // Get the response
        $response = $client->get($url);
          $r = print_r($response, 1);
        // Decode the response body to string
        $data = json_decode((string) $response->getBody(), true); // true = array instead of object
          $d = print_r($data, 1);
        // log
        $log = '<details><summary style="cursor:pointer;">$url</summary><pre>'.$url.'</pre></details>';
        $log .= '<details><summary style="cursor:pointer;">$response</summary><pre>'.$r.'</pre></details>';
        $log .= '<details><summary style="cursor:pointer;">$data</summary><pre>'.$d.'</pre></details>';

        if ($response->getStatusCode() === 200) {
          return self::success_notice( '<code>'.$response->getStatusCode().'</code>&nbsp;'.$response->getReasonPhrase().$log );
        } else {
          return self::error_notice( '<code>'.$response->getStatusCode().'</code>&nbsp;'.$response->getReasonPhrase().$log  );
        }

      } catch (\Exception $exc) {
        self::error_notice($exc->getMessage());
      }
    }

    /**
     *
     * @param type $notice
     */
    public static function info_notice($notice) {
        ?>
        <div class="notice inline notice-info notice-alt is-dismissible"><?php _e( $notice ); ?></div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function success_notice($notice) {
        ?>
        <div class="notice inline notice-success notice-alt is-dismissible"><?php _e( $notice ); ?></div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function warning_notice($notice) {
        ?>
        <div class="notice inline notice-warning notice-alt is-dismissible"><?php _e( $notice ); ?></div>
        <?php
    }
    /**
     *
     * @param type $notice
     */
    public static function error_notice($notice) {
        ?>
        <div class="notice inline notice-error notice-alt is-dismissible"><?php _e( $notice ); ?></div>
        <?php
    }

  }

  New SPHConnection;

}
