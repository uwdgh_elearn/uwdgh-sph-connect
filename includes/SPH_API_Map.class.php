<?php
namespace UWDGH;

if ( !class_exists( 'SPH_API_Map' ) ) {
	
	abstract class SPH_API_Map {

		/**
		 * Returns the SPH Fac Bio JSON API mapping as array
		 */
		public static function map_array() {

			return self::sph_api_map;

		}

		/**
		 * Returns the SPH Fac Bio JSON API mapping as object
		 */
		public static function map() {

			return json_decode(json_encode(self::sph_api_map), FALSE);

		}

		/**
		 * Returns the SPH URLs as object
		 */
		public static function service() {

			return json_decode(json_encode(self::sph_service), FALSE);

		}

		/**
		 * This array maps the SPH base URIs
		 */
		private const sph_service = array(
			'ip_based' => array(
				'production' => array(
					'uri' => 'https://apps.sphcm.washington.edu/json/api/',
				),
				'test' => array(
					'uri' => 'https://appstest.sphcm.washington.edu/json/api/',
				),
			),
			'cert_based' => array(
				'production' => array(
					'uri' => 'https://api.sph.washington.edu/json/api/',
				),
			),
		);

		/**
		 * The array reflects the SPH Fac Bio JSON API
		 * @link https://uwnetid-my.sharepoint.com/personal/sphit_uw_edu/appdocs/Documentation/Fac%20Bio%20JSON.aspx
		 */
		private const sph_api_map = array(
			'endpoint' => array(
				'faclistDept' => array(
					'parameter' => array(
						'dept_ID' => array(
							'required' => true,
							'default' => null,
							'values' =>  array('biostat','envhlth','epi','glbhlth','hserv'),
						),
						'reg' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'adj' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'cli' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true','sal','non'),
						),
						'aff' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'eme' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'res' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'rea' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'non' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'page_num' => array(
							'required' => false,
							'default' => 0,
						),
						'num_items' => array(
							'required' => false,
							'default' => 0,
						),
					),
				),
				'faclistSPH' => array(
					'parameter' => array(
						'dept_ID' => array(
							'required' => true,
							'default' => null,
							'values' =>  array('biostat','envhlth','epi','glbhlth','hserv'),
						),
						'reg' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'adj' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'cli' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'aff' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'eme' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'res' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'rea' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'non' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'page_num' => array(
							'required' => false,
							'default' => 0,
						),
						'num_items' => array(
							'required' => false,
							'default' => 0,
						),
					),
				),
				'faclistSPH_Core' => array(
					'parameter' => array(
						'page_num' => array(
							'required' => false,
							'default' => 0,
						),
						'num_items' => array(
							'required' => false,
							'default' => 0,
						),
					),
				),
				'faclistSPH_Category' => array(
					'parameter' => array(
						'reg' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'adj' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'cli' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'aff' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'eme' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'res' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'rea' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'non' => array(
							'required' => false,
							'default' => 'false',
							'values' =>  array('true'),
						),
						'page_num' => array(
							'required' => false,
							'default' => 0,
						),
						'num_items' => array(
							'required' => false,
							'default' => 0,
						),
					),
				),
				'facDetail' => array(
					'parameter' => array(
						'url_ID' => array(
							'required' => true,
						),
						'web_publ_ID' => array(
							'required' => true,
							'default' => 'SPHCM',
							'values' => array('biostat','envhlth','epi','glbhlth','hserv','SPHCM'),
						),
						'change_hours' => array(
							'required' => false,
							'default' => 0,
						),
					),
				),
				'ceFac' => array(
					'parameter' => array(
						'url_ID' => array(
							'required' => true,
						),
						'max' => array(
							'required' => false,
							'default' => 10,
						),
					),
				),
				'faclistAppt' => array(
					'parameter' => array(
						'dept_ID' => array(
							'required' => true,
							'values' => array('biostat','envhlth','epi','glbhlth','hserv'),
						),
					),
				),
				'facNameSearch' => array(
					'parameter' => array(
						'nameStr' => array(
							'required' => true,
						),
						'max' => array(
							'required' => false,
							'default' => 50,
						),
						'dept_ID' => array(
							'required' => false,
							'default' => 'all',
							'values' => array('biostat','envhlth','epi','glbhlth','hserv'),
						),
					),
				),
				'facRiSearch' => array(
					'parameter' => array(
						'srch_phrase' => array(
							'required' => true,
						),
						'web_publ_ID' => array(
							'required' => false,
							'default' => 'SPHCM',
							'values' => array('biostat','envhlth','epi','glbhlth','hserv','SPHCM'),
						),
						'max' => array(
							'required' => false,
							'default' => 50,
						),
					),
				),
			),
		);
		
	}

}