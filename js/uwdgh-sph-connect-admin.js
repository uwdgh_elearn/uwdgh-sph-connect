// The document ready event executes when the HTML-Document is loaded
// and the DOM is ready.
jQuery(document).ready(function( $ ) {

  // Create a function to pick up the link click
  $(document).on('click', '#uwdgh_sph_connect_try_it_link', function(e){
    e.preventDefault(); // prevent default behaviour of link click
    $(".spinner").addClass("is-active"); 	// add the spinner is-active class before the Ajax posting
    $("textarea[id=uwdgh_sph_connect_try_it_result]").val(''); // empty the result area
    var _input = '';
    _input = $('#uwdgh_sph_connect_try_it').val();	// grab the user input
    if (_input == '') {	// use placeholder if empty input
      _input = $('#uwdgh_sph_connect_try_it').attr('placeholder');
      $('#uwdgh_sph_connect_try_it').val(_input);
    }
    // validate JSON string input
    try {
      _input = JSON.parse(_input);	// create an array of the input string
    } catch (exc) {
      $("textarea[id=uwdgh_sph_connect_try_it_result]").val(exc.message);
			$(".spinner").removeClass("is-active");
      return false;
    } finally {

    }
    var data = {
        action: 'uwdgh_sph_connect_howto_tryit', // This is the PHP function to call - note it must be hooked to AJAX
        uwdgh_sph_connect_try_it:  _input,
    };
    // Post to The Wordpress URL that is stored in UWDGH_SPH_Connect.admin_ajax_url
    jQuery.post(UWDGH_SPH_Connect.admin_ajax_url, data, function(response) {
			// success
			$("textarea[id=uwdgh_sph_connect_try_it_result]").val(response);
		})
			.done(function() {
				// second success
			})
			.fail(function(jqXHR, textStatus, errorThrown) {
				// error
				$("textarea[id=uwdgh_sph_connect_try_it_result]").val(textStatus+'\n'+jqXHR.status+' ('+errorThrown+')');
			})
			.always(function() {
				// finished
				$(".spinner").removeClass("is-active"); 	// remove the spinner after the data has been posted
			});
		
  });
  // clear button event
  $(document).on('click', '#uwdgh_sph_connect_try_it_clear', function(e){
    e.preventDefault(); // prevent default behaviour of link click
    $('#uwdgh_sph_connect_try_it').val('');
    $('#uwdgh_sph_connect_try_it_result').val('');
  });

})


// The window load event executes after the document ready event,
// when the complete page is fully loaded.
jQuery(window).load(function () {

})
